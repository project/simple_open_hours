<?php

namespace Drupal\simple_open_hours\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\time_field\Time;

/**
 * Plugin implementation of the 'simple_open_hours' widget.
 *
 * @FieldWidget(
 *   id = "simple_open_hours_widget",
 *   module = "simple_open_hours",
 *   label = @Translation("Simple Оpen Hours"),
 *   field_types = {
 *     "simple_open_hours"
 *   }
 * )
 */
class SimpleOpenHoursWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'expanded' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['expanded'] = [
      '#title' => $this->t('Show as expanded'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('expanded'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $expanded = $this->getSetting('expanded');
    $summary[] = $this->t('Expanded: @status', ['@status' => ($expanded ? $this->t('Yes') : $this->t('No'))]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [
      '#type' => 'details',
      '#title' => $this->t('Simple open hours'),
      '#open' => $this->getSetting('expanded'),
    ];
    $values = $items->getValue()[$delta];
    $week_days = DateHelper::weekDaysOrdered(DateHelper::weekDays(TRUE));
    $week_days_untranslated = DateHelper::weekDaysOrdered(DateHelper::weekDaysUntranslated());
    foreach ($week_days_untranslated as $key => $day) {
      $element[$day] = [
        '#title' => $week_days[$key],
        '#type' => 'checkbox',
        '#default_value' => $values[$day] ?? 0,
        // Wrapping this way to have flat array of values.
        '#prefix' => '<div class="container-inline">',
      ];
      $element[$day . '_from'] = [
        '#type' => 'time',
        '#default_value' => isset($values[$day . '_from']) ? Time::createFromTimestamp($values[$day . '_from'])->formatForWidget(FALSE) : 0,
      ];
      $element[$day . '_to'] = [
        '#suffix' => '</div>',
        '#type' => 'time',
        '#default_value' => isset($values[$day . '_to']) ? Time::createFromTimestamp($values[$day . '_to'])->formatForWidget(FALSE) : 0,
      ];
    }
    return $element;
  }

}
