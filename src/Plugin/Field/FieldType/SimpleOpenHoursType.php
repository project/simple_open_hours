<?php

namespace Drupal\simple_open_hours\Plugin\Field\FieldType;

use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'simple_open_hours' field type.
 *
 * @FieldType(
 *   id = "simple_open_hours",
 *   label = @Translation("Simple Оpen Hours"),
 *   module = "simple_open_hours",
 *   description = @Translation("Provides the field for open/closed hours."),
 *   default_widget = "simple_open_hours_widget",
 *   default_formatter = "simple_open_hours_formatter"
 * )
 */
class SimpleOpenHoursType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [];
    foreach (DateHelper::weekDaysUntranslated() as $day) {
      $columns[$day] = [
        'description' => $day,
        'type' => 'int',
        'not null' => TRUE,
        'size' => 'tiny',
      ];
      $columns[$day . '_from'] = [
        'type' => 'int',
        'length' => 6,
      ];
      $columns[$day . '_to'] = [
        'type' => 'int',
        'length' => 6,
      ];
    }
    return ['columns' => $columns];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    foreach (DateHelper::weekDaysUntranslated() as $day) {
      $properties[$day] = DataDefinition::create('boolean')
        ->setLabel($day);
      $properties[$day . '_from'] = DataDefinition::create('integer')
        ->setSetting('size', 'small')
        ->setLabel(t('@day time from', ['@day' => $day]));
      $properties[$day . '_to'] = DataDefinition::create('integer')
        ->setSetting('size', 'small')
        ->setLabel(t('@day time to', ['@day' => $day]));
    }
    return $properties;
  }

}
