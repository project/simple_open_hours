<?php

namespace Drupal\simple_open_hours\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\time_field\Time;

/**
 * Plugin implementation of the 'simple_open_hours' formatter.
 *
 * @FieldFormatter(
 *   id = "simple_open_hours_formatter",
 *   label = @Translation("Simple Open Hours"),
 *   field_types = {
 *     "simple_open_hours"
 *   }
 * )
 */
class SimpleOpenHoursFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $element_delta = [];
      $values = $item->getValue();
      $week_days = DateHelper::weekDaysOrdered(DateHelper::weekDays(TRUE));
      $week_days_untranslated = DateHelper::weekDaysOrdered(DateHelper::weekDaysUntranslated());
      foreach ($week_days_untranslated as $key => $day) {
        $element_delta[$day] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['simple-open-hours-wrapper']],
        ];
        $label = $this->getSpan($week_days[$key], 'day');
        if (empty($values[$day]) && !empty($this->getSetting('closed'))) {
          // Show day as "Closed".
          $element_delta[$day][] = [
            $label,
            $this->getSpan($this->t('Closed'), 'closed'),
          ];
          $element_delta[$day]['#attributes']['class'][] = 'closed-wrapper';
        }
        elseif (!empty($values[$day])) {
          // If day checked, show hours.
          $from = Time::createFromTimestamp($values[$day . '_from'])->format($this->getSetting('format'));
          $to = Time::createFromTimestamp($values[$day . '_to'])->format($this->getSetting('format'));
          $element_delta[$day][] = [
            $label,
            $this->getSpan($from . $this->getSetting('separator') . $to, 'hours'),
          ];
        }
        else {
          // There are no hours, do not show the day.
          unset($element_delta[$day]);
        }
      }
      // Add values.
      if (!empty($element_delta)) {
        $elements[$delta] = $element_delta;
      }
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['closed'] = [
      '#title' => $this->t('Show not selected days as "Closed"'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('closed'),
    ];
    $elements['format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time Format'),
      '#default_value' => $this->getSetting('format'),
      '#description' => $this->t('See <a href="https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters" target="_blank">the documentation for PHP date formats</a>.'),
    ];
    $elements['separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Separator'),
      '#default_value' => $this->getSetting('separator'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'format' => 'H:i',
      'closed' => 1,
      'separator' => '-',
    ] + parent::defaultSettings();
  }

  /**
   * Get element renderable array.
   *
   * @param string $value
   *   Span value.
   * @param string $class
   *   Span class.
   *
   * @return array
   *   Renderable array.
   */
  protected function getSpan(string $value, string $class): array {
    return [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $value,
      '#attributes' => ['class' => $class],
    ];
  }

}
