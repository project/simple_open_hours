INTRODUCTION
------------
This module provides 'Simple Open Hours' field that you can add to any entity.

* For a full description of the module please visit the project page:
https://drupal.org/project/simple_open_hours

* To submit bug reports and feature suggestions, or to track changes:
https://drupal.org/project/issues/simple_open_hours


REQUIREMENTS
------------
This module depends on a time_field module (https://www.drupal.org/project/time_field).

CONFIGURATION
-------------
The module has no configuration page. When enabled, the module will add "simple_open_hours" field. To remove field, disable the module and clear caches.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module.

MAINTAINERS
-----------
Current maintainers:
 * Yermolenko Oleksandr - https://www.drupal.org/u/kashandarash
