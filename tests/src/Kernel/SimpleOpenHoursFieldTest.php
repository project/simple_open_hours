<?php

namespace Drupal\Tests\simple_open_hours\Kernel;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Test Simple Open Hours field type.
 *
 * @group datetime
 */
class SimpleOpenHoursFieldTest extends FieldKernelTestBase {

  /**
   * A field storage to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'time_field',
    'simple_open_hours',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Add the field.
    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => mb_strtolower($this->randomMachineName()),
      'entity_type' => 'entity_test',
      'type' => 'simple_open_hours',
      'settings' => [],
    ]);
    $this->fieldStorage->save();
    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'required' => TRUE,
    ]);
    $this->field->save();
    // Add view display.
    $display_options = [
      'type' => 'simple_open_hours_formatter',
      'label' => 'hidden',
      'settings' => [
        'separator' => '-',
        'format' => 'H:i',
        'closed' => TRUE,
      ],
    ];
    EntityViewDisplay::create([
      'targetEntityType' => $this->field->getTargetEntityTypeId(),
      'bundle' => $this->field->getTargetBundle(),
      'mode' => 'default',
      'status' => TRUE,
    ])->setComponent($this->fieldStorage->getName(), $display_options)
      ->save();
    // Add form display.
    $form_options = [
      'type' => 'simple_open_hours_widget',
      'settings' => [
        'expanded' => FALSE,
      ],
    ];
    EntityFormDisplay::create([
      'targetEntityType' => $this->field->getTargetEntityTypeId(),
      'bundle' => $this->field->getTargetBundle(),
      'mode' => 'default',
      'status' => TRUE,
    ])->setComponent($this->fieldStorage->getName(), $form_options)
      ->save();

  }

  /**
   * Tests Simple Open Hours field.
   */
  public function testSimpleOpenHoursFieldValues() {
    $field_name = $this->fieldStorage->getName();
    // Create an entity.
    $from = 42;
    $to = 142;
    $entity = EntityTest::create([
      'name' => $this->randomString(),
      $field_name => [
        'Sunday' => TRUE,
        'Sunday_from' => $from,
        'Sunday_to' => $to,
      ],
    ]);
    // Check predefined values.
    $sunday = $entity->{$field_name}->Sunday;
    $sunday_from = $entity->{$field_name}->Sunday_from;
    $sunday_to = $entity->{$field_name}->Sunday_to;
    $this->assertEquals(TRUE, $sunday);
    $this->assertEquals($from, $sunday_from);
    $this->assertEquals($to, $sunday_to);
    // Check default values.
    $monday = $entity->{$field_name}->Monday;
    $monday_from = $entity->{$field_name}->Monday_from;
    $monday_to = $entity->{$field_name}->Monday_to;
    $this->assertEquals(FALSE, $monday);
    $this->assertEquals(0, $monday_from);
    $this->assertEquals(0, $monday_to);
  }

}
