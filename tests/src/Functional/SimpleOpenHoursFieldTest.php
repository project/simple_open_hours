<?php

namespace Drupal\Tests\simple_open_hours\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests email field functionality.
 *
 * @group field
 */
class SimpleOpenHoursFieldTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'field',
    'node',
    'time_field',
    'simple_open_hours',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A field storage to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create Article.
    $this->drupalCreateContentType(['type' => 'article']);
    $this->webUser = $this->drupalCreateUser([
      'create article content',
      'edit own article content',
    ]);
    $this->drupalLogin($this->webUser);
    // Add the field.
    $field_name = 'simple_open_hours';
    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'simple_open_hours',
      'settings' => [],
    ]);
    $this->fieldStorage->save();
    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'entity_type' => 'node',
      'bundle' => 'article',
      'required' => TRUE,
    ]);
    $this->field->save();
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    // Add form display.
    $form_options = [
      'type' => 'simple_open_hours_widget',
      'settings' => [
        'expanded' => FALSE,
      ],
    ];
    $display_repository->getFormDisplay('node', 'article')
      ->setComponent($field_name, $form_options)
      ->save();

    // Add view display.
    $display_options = [
      'type' => 'simple_open_hours_formatter',
      'label' => 'hidden',
      'settings' => [
        'separator' => 'TEST',
        'format' => 'H:i',
        'closed' => TRUE,
      ],
    ];
    $display_repository->getViewDisplay('node', 'article')
      ->setComponent($field_name, $display_options)
      ->save();
  }

  /**
   * Tests simple open hours field widget.
   *
   * @covers \Drupal\simple_open_hours\Plugin\Field\FieldWidget\SimpleOpenHoursWidget::formElement
   */
  public function testSimpleOpenHoursWidget() {
    $this->drupalGet('node/add/article');
    // Check field wrapper.
    $this->assertSession()->responseContains('Simple open hours');
    $this->assertSession()->responseContains('aria-expanded="false"');
    // Check default value.
    $this->assertSession()->fieldValueEquals("simple_open_hours[0][Tuesday_from]", '00:00');
  }

  /**
   * Tests simple open hours field formatter.
   *
   * @covers \Drupal\simple_open_hours\Plugin\Field\FieldFormatter\SimpleOpenHoursFormatter::viewElements
   */
  public function testSimpleOpenHoursFormatter() {
    // Fill values.
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
      'simple_open_hours[0][Tuesday]' => 1,
      'simple_open_hours[0][Tuesday_from]' => '00:42',
      'simple_open_hours[0][Tuesday_to]' => '03:42',
    ];
    $this->drupalGet('node/add/article');
    // Save form.
    $this->submitForm($edit, 'Save');
    // If form saved correctly there will be node page.
    $this->assertSession()->responseContains('00:42TEST03:42');
    $this->assertSession()->responseContains('<span class="closed">Closed</span>');
  }

}
